import React , {useState } from 'react';
import { View, Text  } from 'react-native';
import Button from '../../components/Button';
import style from './style'

function HomeScreen() {
 

    const [ mood , setMood] = useState(false);

    return (
      <View style={style.container}>
         <Button 
            title={"Home Screen Button"}
            onPressButton={() =>  setMood(mood?false:true)}
         />
         {
             mood ? <Text>Holaaa</Text>:<></>///** NADA */
         }
      </View>
    );
}

export default HomeScreen