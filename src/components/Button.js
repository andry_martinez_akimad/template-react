import * as React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text  } from 'react-native';
import style from './style'

Button.propTypes = {
    title: PropTypes.string ,
    onPressButton: PropTypes.func
}

function Button({title , onPressButton}) {
    return(
        <TouchableOpacity onPress={onPressButton} style={style.Button}>
            <Text style={style.title} >{title}</Text>
        </TouchableOpacity>
    )
}

export default Button