import { StyleSheet, Dimensions } from "react-native";


const { width , height } = Dimensions.get('window')

const styles = StyleSheet.create({
    Button: {
        height:height*0.05,
        backgroundColor: "red",
        padding:7
    },
    title:{
        color:"white",
        fontWeight:"bold",
        fontSize:16
    }
  });

export default styles
  
